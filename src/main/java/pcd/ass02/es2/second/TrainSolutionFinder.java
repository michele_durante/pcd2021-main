package pcd.ass02.es2.second;


import io.vertx.core.AbstractVerticle;
import pcd.ass02.es2.first.TrainLib;

public class TrainSolutionFinder extends AbstractVerticle {

	private TrainLib tl;
	private String from, to, date, time;
	private Controller controller;
	
	public TrainSolutionFinder(String from, String to, String date, String time, Controller c) {
		this.from = from.toUpperCase().replace(" ","%20");
		this.to = to.toUpperCase().replace(" ","%20");
		this.date = date;
		this.time = time;
		this.controller = c;
	}
	
	public void start() {
		log("started");
		tl = new TrainLib(this.getVertx());
		tl
		.getTrainSolutions(from, to, date, time)
		.onSuccess(list -> {
			log("succeeded");
			controller.notifySolutions(list);
		}).onFailure(h -> {
			log("failure: " + h);
		});
		log("done");
	}

	private void log(String msg) {
		System.out.println("" + Thread.currentThread() + " " + msg);
	}	
}
