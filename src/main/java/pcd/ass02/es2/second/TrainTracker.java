package pcd.ass02.es2.second;

/**
 * 
 * @author aricci
 *
 */
public class TrainTracker {
	public static void main(String[] args) {
		Controller controller = new Controller();
        TrainTrackingView view = new TrainTrackingView(controller);
        controller.setView(view);
        view.display();
	}
}
