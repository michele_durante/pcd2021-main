package pcd.ass02.es2.first;

import java.time.LocalTime;

public class TrainStopData {

	private String station;
	private LocalTime programmedTime;
	private LocalTime actualOrPredictedTime;
	private boolean done;

	public TrainStopData(String station, LocalTime plannedTime, LocalTime actualTime, boolean done) {
		this.station = station;
		this.programmedTime = plannedTime;
		this.actualOrPredictedTime = actualTime;
		this.done = done;
		if (actualOrPredictedTime == null) {
			this.done = false;
		}
	}
	
	public String getStation() {
		return station;
	}
	
	public LocalTime getProgrammedTime() {
		return programmedTime;
	}
	
	public LocalTime getActualOrPredictedTime() {
		return actualOrPredictedTime;
	}
	
	public boolean isValid() {
		return !this.station.equals("");
	}
	public boolean isPrediction() {
		return !done;
	}
	
	public String toString() {
		return "station: " + station + " - programmed: " + (programmedTime != null ? programmedTime : "-")  + 
				(done ? " - actual: " : " - predicted: ") + (actualOrPredictedTime != null ? actualOrPredictedTime : "-");
	}
}
