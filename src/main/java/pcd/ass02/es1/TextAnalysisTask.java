package pcd.ass02.es1;

import java.util.concurrent.Callable;

public class TextAnalysisTask implements Callable<Void> {

	private String chunk;
	private SharedData sharedData;
	
	public TextAnalysisTask(String chunk, SharedData sharedData) {
		this.chunk = chunk;
		this.sharedData = sharedData;
	}

	public Void call() {
		logDebug("started - chunk: " + chunk.substring(0, Math.min(20, chunk.length()))+"...");
		String del = "[\\x{201D}\\x{201C}\\s'\", ?.@;:!-]+";
		if (!sharedData.getStopFlag().isSet()) {
			String[] words = chunk.split(del);
			for (String w : words) {
				String w1 = w.trim().toLowerCase();
				if (!w1.equals("") && !sharedData.getWordsToDiscard().containsKey(w1)) {
					sharedData.getMap().add(w1);
				}
			}
		} else {
			logDebug("stopped");
		}
		logDebug("completed - chunk: " + chunk.substring(0, Math.min(20, chunk.length()))+"...");
		return null;
	}

	protected void log(String msg) {
		System.out.println("[ Text Analyser Task ] " + msg);
	}

	protected void logDebug(String msg) {
		System.out.println("[ Text Analyser Task ] " + msg);
	}
	
}
