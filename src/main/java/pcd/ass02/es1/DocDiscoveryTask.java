package pcd.ass02.es1;

import java.io.File;
import java.util.concurrent.*;
import java.util.*;


public class DocDiscoveryTask implements Callable<List<Future<List<Future<Void>>>>> {

	private File startDir;
	private int nDocsFound;
	private SharedData sharedData;
	
	public DocDiscoveryTask(File dir, SharedData sharedData) {
		this.startDir = dir;	
		this.sharedData = sharedData;
	}
	
	public List<Future<List<Future<Void>>>> call() {
		nDocsFound = 0;
		List<Future<List<Future<Void>>>> tasks = new ArrayList<Future<List<Future<Void>>>>();
		if (startDir.isDirectory()) {
			explore(startDir, tasks);
			if (!sharedData.getStopFlag().isSet()) {
				log("job done - " + nDocsFound + " docs found.");
			} else {
				log("stopped.");
			}
		}
		return tasks;
	}
	
	private void explore(File dir, List<Future<List<Future<Void>>>> tasks) {
		if (!sharedData.getStopFlag().isSet()) {
			for (File f: dir.listFiles()) {
				if (f.isDirectory()) {
					explore(f, tasks);
				} else if (f.getName().endsWith(".pdf")) {
					try {
						logDebug("find a new doc: " + f.getName());
						Future<List<Future<Void>>> fut = sharedData.getExecDocLoading().submit(new DocLoaderTask(f, sharedData));
						tasks.add(fut);
						nDocsFound++;
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} 
	}

	protected void log(String msg) {
		System.out.println("[ Doc Discover Task ] " + msg);
	}

	protected void logDebug(String msg) {
		System.out.println("[ Doc Discover Task ] " + msg);
	}

}
