package pcd.ass02.es3;

import java.io.File;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class ChunkStreamBuilder {

	private PDFTextStripper stripper;
	private Flag stopFlag;

	public ChunkStreamBuilder(Flag stopFlag) throws Exception {
		this.stopFlag = stopFlag;
		stripper = new PDFTextStripper();
	}

	public Observable<String> createChunkStream(Observable<File> source) {
		return source
			.observeOn(Schedulers.io()) 
			.flatMap((File doc) -> {
				logDebug(" started - working on " + doc.getName());
				PDDocument document = PDDocument.load(doc);
				AccessPermission ap = document.getCurrentAccessPermission();
				if (ap.canExtractContent()) {
					int nPages = document.getNumberOfPages();
					return Observable.create(emitter -> {
						for (int i = 0; i < nPages; i++) {
							if (!stopFlag.isSet()) {
								stripper.setStartPage(i);
								stripper.setEndPage(i);
								String chunk = stripper.getText(document);
								emitter.onNext(chunk);
							}
						}
						logDebug(" done.");
						document.close();
						emitter.onComplete();
					});
				}
				document.close();
				return Observable.empty();
			});
	}

	protected void log(String msg) {
		System.out.println("[ Doc Loader Stream ] " + msg);
	}

	protected void logDebug(String msg) {
		System.out.println("[ Doc Loader Stream ] " + msg);
	}

}
