package pcd.ass02.es3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;

import io.reactivex.rxjava3.core.Observable;
import pcd.ass1.sol.part1.BasicAgent;


public class Master extends BasicAgent {

	private File configFile;
	private File dir;
	private int numMostFreqWords;
	
	private HashMap<String,String> wordsToDiscard;
	private WordFreqMap map;
	
	private Flag stopFlag;
	
	private View view;
	
	/* performance tuning params */
	
	public Master(File configFile, File dir, int numMostFreqWords, Flag stopFlag, View view) {
		super("master");
		this.configFile = configFile;
		this.dir = dir;
		this.numMostFreqWords = numMostFreqWords;
		this.view = view;
		this.stopFlag = stopFlag;
	}
	
	public void run() {
		log("started.");
		try {
			
			long t0 = System.currentTimeMillis();
			
			map = new WordFreqMap(numMostFreqWords);

			Flag done = new Flag();
			Viewer viewer = new Viewer(map,view,done);
			viewer.start();
			
			loadWordsToDiscard(configFile);

			Observable<File> docs = new DocStreamBuilder(dir, stopFlag).createDocStream();			
			Observable<String> chunks = new ChunkStreamBuilder(stopFlag).createChunkStream(docs);			
			Observable<String> words = new WordStreamBuilder(wordsToDiscard, stopFlag).createWordStream(chunks);
			
			words.subscribe((String w) -> {
				map.add(w);
			}, e -> {
			}, () -> {
				// completion
				
				long t2 = System.currentTimeMillis();
				done.set();
				view.done();
				elabMostFreqWords();
				log("done in " + (t2-t0));
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
		
	private void loadWordsToDiscard(File configFile) {
		try {
			wordsToDiscard = new HashMap<String,String>();
			FileReader fr = new FileReader(configFile);
			BufferedReader br = new BufferedReader(fr);
			br.lines().forEach(w -> {
				wordsToDiscard.put(w, w);
			});			
			fr.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
	}	
	
	private void elabMostFreqWords() {
		Object[] freqs = map.getCurrentMostFreq();
		for (int i = numMostFreqWords-1; i >=0; i--) {
			AbstractMap.SimpleEntry<String, Integer> el = (AbstractMap.SimpleEntry<String, Integer>) freqs[i];
			String key = el.getKey();
			System.out.println(" " + (numMostFreqWords - i) + " - " +  key + " " + el.getValue());
		}		
	}

}
