package pcd.lab02.lost_updates.sol_with_synchmeth;


public class TestCounterSafe {

	public static void main(String[] args) throws Exception {
		int ntimes = Integer.parseInt(args[0]);
		SafeCounter c = new SafeCounter(0);
		Worker w1 = new Worker(c,ntimes);
		Worker w2 = new Worker(c,ntimes);
		Worker w3 = new Worker(c,ntimes);
		Worker w4 = new Worker(c,ntimes);
		Worker w5 = new Worker(c,ntimes);
		Worker w6 = new Worker(c,ntimes);
		Worker w7 = new Worker(c,ntimes);
		Worker w8 = new Worker(c,ntimes);
		
		System.out.println(Integer.MAX_VALUE);

		Cron cron = new Cron();
		cron.start();
		w1.start();
		w2.start();
		w3.start();
		w4.start();
		w5.start();
		w6.start();
		w7.start();
		w8.start();
		w1.join();
		w2.join();
		w3.join();
		w4.join();
		w5.join();
		w6.join();
		w7.join();
		w8.join();
		cron.stop();
		System.out.println("Counter final value: "+c.getValue()+" in "+cron.getTime()+"ms.");
	}
}
